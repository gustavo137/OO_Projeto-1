//Gustavo Afonso/ 30-03-2018
//função do programa : ler um arquivo .pgm e criar uma cópia em .txt para outro arquivo
#include <iostream>
#include <fstream>
#include <string>
#include <vector>
using namespace std;
int main () {
  char largura;
  char altura;
  char max_cinza;
  string line;
  string  copia;
  string copia2[100];
  int i = 0;
  fstream arquivo_original ("barbara.ascii.pgm");
  fstream arquivo_copia ("data.txt");
  if (arquivo_original.is_open())
  {
    while ( getline (arquivo_original,line) )
    {
        cout << line ;
	copia = line;
	copia2[i] = copia;
	arquivo_copia << copia2[i];
	arquivo_copia << "\n";
        i++;
	if (line == "P2"){
		cout << "  formato PGM reconhecido"<<'\n';
		}
		else  if (copia[0] == '#'){
		cout << "  Comentário reconhecido"<<'\n';
		}
		else if(copia[0]!=' ' && copia[2] != ' ' && i == 3 ){
		largura = copia[0];
		altura = copia[2];
		printf("  valores de largura e altura reconhecidos: %c largura , %c altura \n", largura, altura);
		}
                else if(copia[0]!=' '  && i == 4 ){
                max_cinza = copia[0];
                printf("  valor maximo de cinza reconhecido %c\n", max_cinza);
                }
		else{
		cout << '\n';
	}
  }
    arquivo_original.close();
    arquivo_copia.close();
  }
  else cout << "Leitura de arquivo comprometida"; 
  return 0;
}
